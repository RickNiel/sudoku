package database;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;

//DBAccess class for CouchDB
//sets up the connection to Couchdb
//Author Rick Nieling

public class CouchDBAccess {

    private CouchDbClient client;

    private String userName;
    private String password;
    private String databaseName;
    private static boolean CREATE_DB_IF_NOT_EXISTS = true;
    private static String PROTOCOL = "http";
    private static String HOST = "localhost";
    private static int PORT = 5984;

    //constructor
    public CouchDBAccess(String userName, String password, String databaseName){
        CouchDbProperties properties = new CouchDbProperties();

        properties.setDbName(databaseName);
        properties.setUsername(userName);
        properties.setPassword(password);

        properties.setCreateDbIfNotExist(CREATE_DB_IF_NOT_EXISTS);
        properties.setHost(HOST);
        properties.setPort(PORT);
        properties.setProtocol(PROTOCOL);

        client = new CouchDbClient(properties);
    }

    //singleton holder
    private static class SingletonHolder{
        public static CouchDBAccess INSTANCE;
    }

    //get singleton
    public static CouchDbClient getInstance() {
        return SingletonHolder.INSTANCE.client;
    }

    //set singleton
    public static void setInstance(String userName, String password, String database){
        SingletonHolder.INSTANCE = new CouchDBAccess(userName, password, database);
    }

    //shutdown client
    public void shutDown(){
        this.client.shutdown();
    }
}
