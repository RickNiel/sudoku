package database;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import model.Score;
import org.lightcouch.CouchDbClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

//DAO class for querying the Sudoku results from couchDB
//Author Rick Nieling

public class ResultsCouchDAO {

    private CouchDbClient couchDbClient;
    private Gson gson;
    private File file = new File("src/main/resources/scoreJson.txt");

    //constructor
    public ResultsCouchDAO(CouchDbClient couchDbClient) {
        this.couchDbClient = couchDbClient;
        gson = new Gson();
    }

    //setter
    public void setCouchDbClient(CouchDbClient couchDbClient) {
        this.couchDbClient = couchDbClient;
    }

    //singleton
    private static class SingletonHolder {
        public static ResultsCouchDAO INSTANCE;
    }

    //get singleton instance
    public static ResultsCouchDAO getInstance(CouchDbClient couchDbClient) {
        if (SingletonHolder.INSTANCE == null) {
            SingletonHolder.INSTANCE = new ResultsCouchDAO(couchDbClient);
        }
        SingletonHolder.INSTANCE.setCouchDbClient(couchDbClient);
        return SingletonHolder.INSTANCE;
    }

    //set singleton instance
    public static void setInstance(CouchDbClient couchDbClient) {
        ResultsCouchDAO.SingletonHolder.INSTANCE = new ResultsCouchDAO(couchDbClient);
    }

    //save the score of a user to CouchDB
    public void saveScore(Score score) {
        couchDbClient.save(score);

        //alternative way of saving a score:
//        String jsonString = gson.toJson(score);
//        System.out.println(jsonString);
//        JsonParser parser = new JsonParser();
//        JsonObject jsonObject = parser.parse(jsonString).getAsJsonObject();
    }

    //get all scores from CouchDB
    public ArrayList<Score> getAllScores() {
        //init arraylist
        ArrayList<Score> scores = new ArrayList<>();

        //query all docs from couchDB
        List<JsonObject> jsonScores = couchDbClient.view("_all_docs").includeDocs(true).query(JsonObject.class);

        //parse every jsonobject to a score object
        for (JsonObject jsonScore : jsonScores) {
            Score score = gson.fromJson(jsonScore, Score.class);
            scores.add(score);
        }

        //return all scores array
        return scores;
    }

    //get all scores from a specific user
    public ArrayList<Score> getScoresByUser(String username) {
        //init
        ArrayList<Score> scores = new ArrayList<>();

        //query all results from a specific user based on the username through a view
        List<JsonObject> jsonScores = couchDbClient.view("practice/scores-per-user").key(username).query(JsonObject.class);

        //for every jsonobject, get the results based on the ID
        for (JsonObject jsonScore : jsonScores) {
            //get id
            JsonElement id = jsonScore.get("id");
            String idString = id.getAsString();

            //find all results with ID
            JsonObject jsonScoreFound = couchDbClient.find(JsonObject.class, idString);

            //parse jsonobject to score object
            Score score = gson.fromJson(jsonScoreFound, Score.class);
            scores.add(score);

        }

        //return all scores array
        return scores;
    }


    //get best score for user
    public Score getBestScoreForUser(String user) {
        ArrayList<Score> scores = getScoresByUser(user);
        Collections.sort(scores);
        if (scores.size() > 0) {
            return scores.get(0);
        } else {
            return null;
        }
    }


    //get specific score of user
    public Score getScore(String user) {
        ArrayList<Score> scores = ResultsCouchDAO.getInstance(CouchDBAccess.getInstance()).getScoresByUser(user);
        String id = user + "_" + (scores.size());
        return couchDbClient.find(Score.class, id);
    }


    //write result to json file
    public void writeToJson(Score score) {
        try {
            PrintWriter pw = new PrintWriter(file);
            String scoreString = gson.toJson(score);
            pw.println(scoreString);
            pw.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }


    //get all results of specific user from jsonfile
    public ArrayList<Score> getFromJson(String username) {
        ArrayList<Score> results = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                Score scoreFromJson = gson.fromJson(scanner.nextLine(), Score.class);
                if (scoreFromJson.getUserName().equals(username)) {
                    results.add(scoreFromJson);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return results;
    }
}