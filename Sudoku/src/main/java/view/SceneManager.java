package view;


import controller.GameSceneController;
import controller.InstructionController;
import controller.WelcomeScreenController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

//Class manages the loading of different scenes
//Author Rick Nieling

public class SceneManager {

    private Stage primaryStage;
    private String difficulty;
    private String user;

    public SceneManager(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public FXMLLoader getScene(String fxml) {
        Scene scene;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
            Parent root = loader.load();
            scene = new Scene(root);
            primaryStage.setScene(scene);
            return loader;
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return null;
        }
    }

    public void showWelcomeScene() {
        FXMLLoader fxmlLoader = getScene("/view/fxml/welcomeScene.fxml");
        WelcomeScreenController welcomeScreenController = fxmlLoader.getController();
        welcomeScreenController.setup();
    }

    public void showInstructionScene() {
        FXMLLoader fxmlLoader = getScene("/view/fxml/instructionScene.fxml");
        InstructionController instructionController = fxmlLoader.getController();
        instructionController.setup();
    }

    public void showGameScene() {
        FXMLLoader fxmlLoader = getScene("/view/fxml/gameScene.fxml");
        GameSceneController gameSceneController = fxmlLoader.getController();
        gameSceneController.setup(user, difficulty);
    }


    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
