package controller;

import database.CouchDBAccess;
import database.ResultsCouchDAO;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import model.Score;
import model.SudokuGenerator;

import java.util.function.UnaryOperator;

//Controller for the game scene
//class builds the sudoku board, checks results and saves results to DB
//Author: Rick Nieling

public class GameSceneController {

    @FXML
    private GridPane gridPane;
    @FXML
    private Label timeLabel;

    private Score score;
    private long startTime;
    private long elapsedMillis;
    private int[][] numbers = new int[9][9];
    private TextField[][] textFields = new TextField[9][9];
    private int[][] solution = new int[9][9];

    private final int EASY_DIFFICULTY = 10;
    private final int MEDIUM_DIFFICULTY = 15;
    private final int HARD_DIFFICULTY = 20;
    private final int DEFAULT_DIFFICULTY = 15;


    //setup scene
    public void setup(String user, String difficulty) {
        //init score
        score = new Score();
        initScore(user, difficulty);

        //start timer
        startTime = System.currentTimeMillis();
        startTimer();

        //setup board
        setupBoard(difficulty);
    }

    public void initScore(String user, String difficulty) {
        score.setDifficulty(difficulty);
        score.setUserName(user);
        score.setId();
    }

    //start timer
    public void startTimer() {
        new AnimationTimer() {
            @Override
            public void handle(long l) {
                elapsedMillis = System.currentTimeMillis() - startTime;
                timeLabel.setText(Long.toString(elapsedMillis / 1000));
            }
        }.start();
    }

    //setup sudoku board
    public void setupBoard(String difficulty) {
        //define difficulty
        int missingNumbers = switch (difficulty) {
            case "Easy" -> EASY_DIFFICULTY;
            case "Medium" -> MEDIUM_DIFFICULTY;
            case "Hard" -> HARD_DIFFICULTY;
            default -> DEFAULT_DIFFICULTY;
        };

        //generate random sudoku
        SudokuGenerator sudokuGenerator = new SudokuGenerator(9, missingNumbers);
        sudokuGenerator.fillValues();
        solution = sudokuGenerator.getSudoku();

        //generate the board and fill numbers
        generateBoard(solution);
    }

    public void generateBoard(int[][] solution) {
        //add a textfield for every row/column
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {


                //create new textfield
                TextField textField = new TextField();

                //set the sudoku number in the textfield. leave textfield empty if 0 is found (empty value)
                if (solution[row][col] != 0) {
                    textField.setText(Integer.toString(solution[row][col]));
                    textField.setDisable(true);
                    textField.setOpacity(1);
                    textField.setId("filledTextField");
                } else {
                    textField.setId("emptyTextField");
                }

                //add textfield to gridpane
                gridPane.add(textField, col, row);
                textFields[row][col] = textField;

                //create a filter textFormatter that detects the changes in the textfield and
                //only allows 1 - 9 or a single character to be entered
                UnaryOperator<TextFormatter.Change> filter = change -> {
                    String text = change.getText();
                    String textLength = change.getControlNewText();

                    if (text.matches("[1-9]*")) {
                        if (textLength.length() <= 1) {
                            return change;
                        }
                    }
                    return null;
                };

                //create textformatter based on the defined filter
                TextFormatter<String> textFormatter = new TextFormatter<String>(filter);
                //add text formatter to the textfield
                textField.setTextFormatter(textFormatter);

            }


        }
    }


    public void doResetBoard(ActionEvent actionEvent) {
        generateBoard(solution);
    }

    //save the user's results to CouchDB
    public void saveResults() {
        score.setTime(elapsedMillis / 1000);
        ResultsCouchDAO.getInstance(CouchDBAccess.getInstance()).saveScore(score);
        ResultsCouchDAO.getInstance(CouchDBAccess.getInstance()).writeToJson(score);
    }

    //handle button press
    public void doCheckSolution(ActionEvent actionEvent) {
        //parse all text values to integer array
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 9; col++) {
                if (textFields[row][col].getText().trim().length() > 0) {
                    numbers[row][col] = Integer.parseInt(textFields[row][col].getText());
                }
            }
        }

        //check if the solution is valid
        if (isValid(numbers)) {
            saveResults();
            showCorrectAlert();
            Main.getSceneManager().showWelcomeScene();
        } else {
            showIncorrectAlert();
        }
    }

    //check if solution is valid
    public boolean isValid(int[][] numbers) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (!isValid(i, j, numbers)) {
                    return false;
                }
            }
        }
        return true;
    }

    //check if solution is valid
    public boolean isValid(int i, int j, int[][] numbers) {
        //check columns
        for (int col = 0; col < 9; col++) {
            if (col != j && numbers[i][col] == numbers[i][j]) {
                return false;
            }
        }

        //check rows
        for (int row = 0; row < 9; row++) {
            if (row != i && numbers[row][j] == numbers[i][j]) {
                return false;
            }
        }

        //check small squares
        for (int row = (i / 3) * 3; row < (i / 3) * 3 + 3; row++) {
            for (int col = (j / 3) * 3; col < (j / 3) * 3 + 3; col++) {
                if (!(row == i && col == j) && numbers[row][col] == numbers[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    //alert for incorrect solution
    private void showIncorrectAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Solution check");
        alert.setHeaderText("Solution incorrect.");
        alert.showAndWait();
    }

    //alert for correct solution
    private void showCorrectAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Solution check");
        alert.setHeaderText("Congratulations! You've solved the Sudoku in " + (elapsedMillis / 1000) + " seconds!");
        alert.showAndWait();
    }

    //return to main scene
    public void doQuit() {
        Main.getSceneManager().showWelcomeScene();
    }

    //test numbers (correct sudoku)
    public void setTest() {
        int[][] numbers = {
                {8, 2, 7, 1, 5, 4, 3, 9, 6},
                {9, 6, 5, 3, 2, 7, 1, 4, 8},
                {3, 4, 1, 6, 8, 9, 7, 5, 2},
                {5, 9, 3, 4, 6, 8, 2, 7, 1},
                {4, 7, 2, 5, 1, 3, 6, 8, 9},
                {6, 1, 8, 9, 7, 2, 4, 3, 5},
                {7, 8, 6, 2, 3, 5, 9, 1, 4},
                {1, 5, 4, 7, 9, 6, 8, 2, 3},
                {2, 3, 9, 8, 4, 1, 5, 6, 7}
        };
    }
}


//    Timer timer = new Timer();
//
//    int finalRow = row;
//    int finalCol = col;
//
//                timer.schedule(new TimerTask(){
//@Override
//public void run(){
//
//        },10);