package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

//controller class for instructionScene
//shows the rules of a Sudoku
//Author: Rick Nieling

public class InstructionController {

    @FXML
    private Label instructionLabel;

    //setup the scene
    public void setup() {
        setupInstructionLabel();
    }

    //set instruction text
    public void setupInstructionLabel(){
        instructionLabel.setText("Sudoku is played on a grid of 9 x 9 spaces. Within the rows and columns are \n" +
                                    "9 “squares (made up of 3 x 3 spaces). Each row, column and square (9 spaces each) \n" +
                                    "needs to be filled out with the numbers 1-9, without repeating any numbers \n" +
                                    "within the row, column or square.");
    }

    //start game; show game scene
    public void doStartGame(ActionEvent actionEvent) {
        Main.getSceneManager().showGameScene();
    }


}
