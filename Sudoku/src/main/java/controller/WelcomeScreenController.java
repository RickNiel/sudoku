package controller;

import database.CouchDBAccess;
import database.ResultsCouchDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Score;
import java.util.ArrayList;
import java.util.Collections;

//Controller class for welcomeScene
//Allows user to enter name, select difficulty and start game
//Shows all scores currently in CouchDB
//Author Rick Nieling

public class WelcomeScreenController {

    @FXML
    private Button startButton;
    @FXML
    private TextField nameTextField;
    @FXML
    private MenuButton difficultyMenuButton;
    @FXML
    private ListView<Score> easyListView;
    @FXML
    private ListView<Score> mediumListView;
    @FXML
    private ListView<Score> hardListView;
    @FXML
    private TextField searchTextField;
    @FXML
    private ListView<Score> highScoresPerUserListView;
    @FXML
    private Button findAllScores, findBestScore;


    //setup scene
    public void setup() {
        setupMenuItems();
        fillHighscores();
        bindButton();
    }

    //populate menu items and add event listeners
    private void setupMenuItems() {
        //init
        MenuItem menuItemEasy = new MenuItem("Easy");
        MenuItem menuItemMedium = new MenuItem("Medium");
        MenuItem menuItemHard = new MenuItem("Hard");

        //add items
        difficultyMenuButton.getItems().addAll(menuItemEasy, menuItemMedium, menuItemHard);

        //set default text
        difficultyMenuButton.setText("Easy");

        //add listeners
        menuItemEasy.setOnAction(event -> difficultyMenuButton.setText("Easy"));
        menuItemMedium.setOnAction(event -> difficultyMenuButton.setText("Medium"));
        menuItemHard.setOnAction(event -> difficultyMenuButton.setText("Hard"));
    }

    //fill high scores ListViews with data from CouchDB
    private void fillHighscores() {
        //get all scores from DB
        ArrayList<Score> scores = ResultsCouchDAO.getInstance(CouchDBAccess.getInstance()).getAllScores();
        //sort scores
        Collections.sort(scores);

        //fill ListViews based on difficulty
        for (Score score : scores) {
            if (score.getDifficulty() != null) {
                switch (score.getDifficulty().toLowerCase()) {
                    case "easy":
                        easyListView.getItems().add(score);
                        break;
                    case "medium":
                        mediumListView.getItems().add(score);
                        break;
                    case "hard":
                        hardListView.getItems().add(score);
                        break;
                }
            }
        }
    }

    //bind buttons
    private void bindButton() {
        startButton.disableProperty().bind(nameTextField.textProperty().isEmpty()
                .or(difficultyMenuButton.armedProperty()));
        findAllScores.disableProperty().bind(searchTextField.textProperty().isEmpty());
        findBestScore.disableProperty().bind(searchTextField.textProperty().isEmpty());
    }

    //handle start game button press
    public void doStartGame(ActionEvent actionEvent) {
        //init user and difficulty
        String user = nameTextField.getText();
        String difficulty = difficultyMenuButton.getText();

        //set difficulty and user
        Main.getSceneManager().setDifficulty(difficulty);
        Main.getSceneManager().setUser(user);

        //show next scene
        Main.getSceneManager().showInstructionScene();
    }

    //find scores of specific user
    public void doFindAllScores(ActionEvent actionEvent) {
        //reset listview
        highScoresPerUserListView.getItems().clear();

        //get username from search text field
        String username = searchTextField.getText().trim();

        //get all scores of user and add to Listview.
        //if none found: show alert
        ArrayList<Score> scores = ResultsCouchDAO.getInstance(CouchDBAccess.getInstance()).getScoresByUser(username);
        if(!scores.isEmpty()){
            Collections.sort(scores);
            highScoresPerUserListView.getItems().addAll(scores);
        } else{
            showNoScoresFoundAlert();
        }
    }

    //find best score of specific user
    public void doFindBestScore(ActionEvent actionEvent) {
        //reset listview
        highScoresPerUserListView.getItems().clear();

        //get username from search text field
        String username = searchTextField.getText().trim();

        //get best score of user and add to Listview.
        //if none found: show alert
        Score score = ResultsCouchDAO.getInstance(CouchDBAccess.getInstance()).getBestScoreForUser(username);
        if(score != null){
            highScoresPerUserListView.getItems().add(score);
        } else{
            showNoScoresFoundAlert();
        }
    }

    //alert no user found
    private void showNoScoresFoundAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Search error");
        alert.setHeaderText("No scores found for username: " + searchTextField.getText());
        alert.show();
    }


}
