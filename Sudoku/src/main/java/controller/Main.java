package controller;

import database.CouchDBAccess;
import javafx.application.Application;
import javafx.stage.Stage;
import view.SceneManager;


public class Main extends Application {

    private static SceneManager sceneManager = null;
    private static Stage primaryStage = null;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        CouchDBAccess.setInstance("admin", "admin", "sudoku_results");

        Main.primaryStage = primaryStage;
        primaryStage.setTitle("Sudoku application");
        getSceneManager().showWelcomeScene();
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        CouchDBAccess.getInstance().shutdown();
        super.stop();
    }

    public static SceneManager getSceneManager() {
        if (sceneManager == null) {
            sceneManager = new SceneManager(primaryStage);
        }
        return sceneManager;
    }
}
