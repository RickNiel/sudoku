package model;


/*
Class contains information for a score entity
Author: Rick Nieling
*/

import database.CouchDBAccess;
import database.ResultsCouchDAO;

import java.util.ArrayList;

public class Score implements Comparable<Score> {

    private String _id;

    private String userName;
    private long time;
    private String difficulty;

    //all args constructor
    public Score(String userName, long time, String difficulty) {
        this.userName = userName;
        this.time = time;
        this.difficulty = difficulty;
        setId();
    }

    //no args constructor
    public Score() {

    }

    public void setId() {
        ArrayList<Score> scores = ResultsCouchDAO.getInstance(CouchDBAccess.getInstance()).getScoresByUser(userName);

        this._id = userName+"_"+(scores.size()+1);

    }


    //getters & setters
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String get_id() {
        return _id;
    }

    //to string
    @Override
    public String toString() {
        return String.format("username: %s - time: %d", userName, time);
    }

    //compare on time
    @Override
    public int compareTo(Score otherScore) {
        return Long.compare(this.time, otherScore.getTime());
    }
}
